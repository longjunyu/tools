USE winetestbot;

ALTER TABLE VMs
  ADD MissionCaps VARCHAR(512) NOT NULL
      AFTER Missions;
