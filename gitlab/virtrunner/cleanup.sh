#!/bin/bash
# Cleanup our VM
set -e
zdir=`dirname "$0"`
source $zdir/config

virsh destroy $DOMAIN
